lazy val core = (project in file("core")).settings(
  organization := "tf.bug",
  name := "aoc",
  version := "0.1.0",
  scalaVersion := "3.0.0-M2",
  libraryDependencies ++= Seq(
    "org.typelevel" %% "cats-core" % "2.3.0",
    "org.typelevel" %% "cats-effect" % "3.0.0-M4",
    "co.fs2" %% "fs2-core" % "3.0.0-M6",
    "co.fs2" %% "fs2-io" % "3.0.0-M6",
    "org.typelevel" %% "cats-parse" % "0.1-31-923a513",
  ),
)
