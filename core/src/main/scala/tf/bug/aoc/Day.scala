package tf.bug.aoc

import cats._
import cats.effect._
import fs2._

trait Day {
  def part1[F[_]: Async]: Pipe[F, String, String]
  def part2[F[_]: Async]: Pipe[F, String, String]
}
