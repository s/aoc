package tf.bug.aoc.y2020

import tf.bug.aoc.{Day, Year}

object y2020 extends Year {

  override def days: Map[Int, Day] = Map(
    1 -> Day1,
    2 -> Day2,
    3 -> Day3,
    4 -> Day4,
    5 -> Day5,
    6 -> Day6,
  )

}
