package tf.bug.aoc

trait Year {
  def days: Map[Int, Day]
}
