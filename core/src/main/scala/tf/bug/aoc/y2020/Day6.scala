package tf.bug.aoc.y2020

import cats.effect._
import cats.syntax.all._
import fs2._
import tf.bug.aoc.Day

object Day6 extends Day {

  override def part1[F[_]: Async]: Pipe[F, String, String] =
    parts(_ | _)

  override def part2[F[_]: Async]: Pipe[F, String, String] =
    parts(_ & _)

  def parts[F[_]: Async](reduction: (Set[Char], Set[Char]) => Set[Char]): Pipe[F, String, String] =
    (lines: Stream[F, String]) => {
      lines.groupAdjacentBy(_.nonEmpty).collect {
        case (true, strings) =>
          strings
            .map(_.toSet)
            .reduceLeftOption(reduction)
            .getOrElse(Set.empty[Char])
      }.map(_.size).foldMonoid.map(_.show)
    }

}
