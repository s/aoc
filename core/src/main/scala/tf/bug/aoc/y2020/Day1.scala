package tf.bug.aoc.y2020

import cats._
import cats.effect._
import cats.syntax.all._
import fs2._
import java.nio.charset.StandardCharsets
import scala.util.Try
import tf.bug.aoc.Day

object Day1 extends Day {

  override def part1[F[_]: Async]: Pipe[F, String, String] =
    parts[F](2)

  override def part2[F[_]: Async]: Pipe[F, String, String] =
    parts[F](3)

  def parts[F[_]: Sync](nums: Int): Pipe[F, String, String] =
    (lines: Stream[F, String]) => {
      val ints: F[Vector[Int]] = lines
        .map(_.toInt)
        .compile.toVector
      val result: F[Option[Int]] = ints.map(sumTargetProduct(2020, nums, _))
      Stream.eval(result).map(_.show)
    }
  
  def sumTargetProduct(target: Int, num: Int, ints: Vector[Int]): Option[Int] =
    ints.combinations(num).collectFirst {
      case v if v.sum == target => v.product
    }

}
